import { IsNotEmpty, IsNumber, Length } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(4, 32)
  name: string;
  @IsNotEmpty()
  @IsNumber()
  age: number;
  @IsNotEmpty()
  @Length(1, 10)
  tel: string;
  @IsNotEmpty()
  @Length(1, 1)
  gender: string;
}
