import { IsNotEmpty, IsNumber, IsPositive } from 'class-validator';

class CreatedOrderItemDto {
  @IsPositive()
  @IsNotEmpty()
  @IsNumber()
  productId: number;
  @IsPositive()
  @IsNotEmpty()
  @IsNumber()
  amount: number;
}
export class CreateOrderDto {
  @IsPositive()
  @IsNotEmpty()
  @IsNumber()
  customerId: number;
  @IsNotEmpty()
  orderItem: CreatedOrderItemDto[];
}
